package com.example.examenc2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA = " ,";
    private static final String SQL_CREATE_PRODUCTO = "CREATE TABLE " + DefinirTabla.Producto.TABLE_NAME + " (" + DefinirTabla.Producto._ID + " INTEGER PRIMARY KEY" + COMMA + DefinirTabla.Producto.NOMBRE + TEXT_TYPE + COMMA + DefinirTabla.Producto.MARCA + TEXT_TYPE + COMMA + DefinirTabla.Producto.PRECIO + INTEGER_TYPE + COMMA + DefinirTabla.Producto.PERECEDERO + INTEGER_TYPE + ")";
    private static final String SQL_DELETE_PRODUCTO = "DROP TABLE IF EXISTS " + DefinirTabla.Producto.TABLE_NAME;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "sistema.db";

    public DbHelper(@Nullable Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(SQL_CREATE_PRODUCTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL(SQL_DELETE_PRODUCTO);
        onCreate(db);
    }

}
