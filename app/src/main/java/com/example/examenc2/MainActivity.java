package com.example.examenc2;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private SistemaProductos db;
    private long id;

    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup radioGroup;
    private RadioButton rbPerecedero;
    private RadioButton rbNoPerecedero;

    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnIrEditar;
    private Button btnNuevo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCodigo = findViewById(R.id.edtIdCreate);
        txtNombre = findViewById(R.id.edtNombreCreate);
        txtMarca = findViewById(R.id.edtMarcaCreate);
        txtPrecio = findViewById(R.id.edtPrecioCreate);

        radioGroup = findViewById(R.id.rdCreateGroup);
        rbNoPerecedero = findViewById(R.id.rbNoPerecederoCreate);
        rbPerecedero = findViewById(R.id.rbPerecederoCreate);

        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnIrEditar = findViewById(R.id.btnEditar);
        btnNuevo = findViewById(R.id.btnNuevo);

        db = new SistemaProductos(MainActivity.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@Nullable View v) {
                db.openDatabase();
                Producto t = db.get(Long.parseLong(txtCodigo.getText().toString()));
                db.close();
                if(txtCodigo.getText().toString().matches("") || txtNombre.getText().toString().matches("") || t != null){
                    Toast.makeText(MainActivity.this, "Favor de rellenar los campos", Toast.LENGTH_SHORT).show();
                }
                else{
                    Producto p = new Producto();
                    p.set_ID(Long.parseLong(txtCodigo.getText().toString()));
                    p.setNombre(txtNombre.getText().toString());
                    p.setMarca(txtMarca.getText().toString());
                    if(!txtPrecio.getText().toString().matches("")) {
                        p.setPrecio(Long.parseLong(txtPrecio.getText().toString()));
                    }
                    else{
                        p.setPrecio(0);
                    }
                    if(rbPerecedero.isChecked()){
                        p.setPerecedero(1);
                    }
                    else{
                        p.setPerecedero(0);
                    }
                    db.openDatabase();
                    db.insert(p);
                    Toast.makeText(MainActivity.this, "El producto se ha insertado", Toast.LENGTH_SHORT).show();
                    db.close();
                    limpiar();
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnIrEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, productoActivity.class);
                startActivity(i);
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

    }

    public void limpiar(){
        txtCodigo.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
        rbPerecedero.toggle();
    }

}
