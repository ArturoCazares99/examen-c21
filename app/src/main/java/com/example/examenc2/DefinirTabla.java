package com.example.examenc2;

import android.provider.BaseColumns;

public class DefinirTabla {
    public static abstract class Producto implements BaseColumns{
        public static final String TABLE_NAME="productos";
        public static final String NOMBRE="nombre", MARCA="marca", PRECIO="precio", PERECEDERO="perecedero";
    }
}
