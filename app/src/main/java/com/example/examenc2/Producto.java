package com.example.examenc2;

public class Producto {
    private long _ID;
    private String nombre;
    private String marca;
    private long precio;
    private int perecedero;

    public Producto(long _ID, String nombre, String marca, long precio, int perecedero) {
        this.set_ID(_ID);
        this.setNombre(nombre);
        this.setMarca(marca);
        this.setPrecio(precio);
        this.setPerecedero(perecedero);
    }

    public Producto(Producto otro) {
        this.set_ID(otro.get_ID());
        this.setNombre(otro.getNombre());
        this.setMarca(otro.getMarca());
        this.setPrecio(otro.getPrecio());
        this.setPerecedero(otro.getPerecedero());
    }

    public Producto() {
    }


    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public long getPrecio() {
        return precio;
    }

    public void setPrecio(long precio) {
        this.precio = precio;
    }

    public int getPerecedero() {
        return perecedero;
    }

    public void setPerecedero(int perecedero) {
        this.perecedero = perecedero;
    }
}
