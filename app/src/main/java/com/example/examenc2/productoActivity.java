package com.example.examenc2;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import static java.lang.String.valueOf;

public class productoActivity extends AppCompatActivity {
    private SistemaProductos db;

    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup radioGroup;
    private RadioButton rbPerecedero;
    private RadioButton rbNoPerecedero;

    private Button btnBuscar;
    private Button btnBorrar;
    private Button btnActualizar;
    private Button btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        txtCodigo = findViewById(R.id.edtIdEdit);
        txtNombre = findViewById(R.id.edtNombreEdit);
        txtMarca = findViewById(R.id.edtMarcaEdit);
        txtPrecio = findViewById(R.id.edtPrecioEdit);

        radioGroup = findViewById(R.id.rdEditGroup);
        rbNoPerecedero = findViewById(R.id.rbNoPerecederoEdit);
        rbPerecedero = findViewById(R.id.rbPerecederoEdit);

        btnBuscar = findViewById(R.id.btnBuscar);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnCerrar = findViewById(R.id.btnCerrar);

        db = new SistemaProductos(productoActivity.this);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                if(db.get(Long.parseLong(txtCodigo.getText().toString())) == null || txtCodigo.getText().toString().matches("")){
                    Toast.makeText(productoActivity.this, "No se encontro el producto", Toast.LENGTH_SHORT).show();
                    db.close();
                }
                else{
                    db.close();
                    db.openDatabase();
                    Producto p = db.get(Long.parseLong(txtCodigo.getText().toString()));
                    db.close();
                    txtCodigo.setText(valueOf(p.get_ID()));
                    txtNombre.setText(p.getNombre());
                    txtMarca.setText(p.getMarca());
                    txtPrecio.setText(valueOf(p.getPrecio()));
                    if(p.getPerecedero() == 1){
                        rbPerecedero.toggle();
                    }
                    else{
                        rbNoPerecedero.toggle();
                    }
                }
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                db.delete(Long.parseLong(txtCodigo.getText().toString()));
                db.close();
                limpiar();
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@Nullable View v) {
                if(txtCodigo.getText().toString().matches("") || txtNombre.getText().toString().matches("")){
                    Toast.makeText(productoActivity.this, "Favor de rellenar los campos", Toast.LENGTH_SHORT).show();
                }
                else{
                    Producto p = new Producto();
                    p.set_ID(Long.parseLong(txtCodigo.getText().toString()));
                    p.setNombre(txtNombre.getText().toString());
                    p.setMarca(txtMarca.getText().toString());
                    p.setPrecio(Integer.parseInt(txtPrecio.getText().toString()));
                    if(rbPerecedero.isChecked()){
                        p.setPerecedero(1);
                    }
                    else{
                        p.setPerecedero(0);
                    }
                    db.openDatabase();
                    db.update(p, Long.parseLong(txtCodigo.getText().toString()));
                    Toast.makeText(productoActivity.this, "El producto se ha Actualizado", Toast.LENGTH_SHORT).show();
                    db.close();
                    limpiar();
                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void limpiar(){
        txtCodigo.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
        rbPerecedero.toggle();
    }
}