package com.example.examenc2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class SistemaProductos {
    private Context context;
    private DbHelper dbHelper;
    private SQLiteDatabase db;

    private String [] columnToRead = new String[]{
            DefinirTabla.Producto._ID,
            DefinirTabla.Producto.NOMBRE,
            DefinirTabla.Producto.MARCA,
            DefinirTabla.Producto.PRECIO,
            DefinirTabla.Producto.PERECEDERO
    };

    public SistemaProductos(Context context){
        this.context = context;
        dbHelper = new DbHelper(this.context);
    }

    public boolean check(long id){
        if(get(id) == null){
            return true;
        }
        else{
            return false;
        }
    }

    public void openDatabase(){
        db = dbHelper.getWritableDatabase();
    }

    public long insert(Producto p){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto._ID, p.get_ID());
        values.put(DefinirTabla.Producto.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Producto.MARCA, p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Producto.PERECEDERO, p.getPerecedero());

        return db.insert(DefinirTabla.Producto.TABLE_NAME, null, values);
    }

    public long update(Producto p, long id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Producto.MARCA, p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Producto.PERECEDERO, p.getPerecedero());

        String criterio = DefinirTabla.Producto._ID + " = " + id;

        return db.update(DefinirTabla.Producto.TABLE_NAME, values, criterio, null);
    }

    public int delete(long id){
        String criterio = DefinirTabla.Producto._ID + " = " + id;

        return db.delete(DefinirTabla.Producto.TABLE_NAME, criterio, null);
    }

    public Producto read(Cursor cursor){
        Producto p = new Producto();

        p.set_ID(cursor.getInt(0));
        p.setNombre(cursor.getString(1));
        p.setMarca(cursor.getString(2));
        p.setPrecio(cursor.getInt(3));
        p.setPerecedero(cursor.getInt(4));

        return p;
    }

    public Producto get(long id){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Producto p = null;
        Cursor cursor = db.query(DefinirTabla.Producto.TABLE_NAME,columnToRead, DefinirTabla.Producto._ID + " = ? ", new String[] {String.valueOf(id)}, null,null,null);
        if(cursor.moveToFirst() == true){
            p = read(cursor);
        }
        cursor.close();
        return p;
    }

    public void close(){
        dbHelper.close();
    }
}
